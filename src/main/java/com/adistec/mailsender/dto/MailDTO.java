package com.adistec.mailsender.dto;

import java.util.ArrayList;
import java.util.List;

public class MailDTO {
    private String[] sentToEmail = new String[]{};
    private String subject;
    private String template;
    private String from;
    private String templateVariables;
    private String inlines;
    private String[] copyTo = new String[]{};
    private String[] cco = new String[]{};
    private String content;

    public String[]  getSentToEmail() {
        return sentToEmail;
    }

    public void setSentToEmail(String[] sentToEmail) {
        this.sentToEmail = sentToEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTemplateVariables() {
        return templateVariables;
    }

    public void setTemplateVariables(String templateVariables) {
        this.templateVariables = templateVariables;
    }

    public String getInlines() {
        return inlines;
    }

    public void setInlines(String inlines) {
        this.inlines = inlines;
    }

    public String[]  getCopyTo() {
        return copyTo;
    }

    public void setCopyTo(String[]  copyTo) {
        this.copyTo = copyTo;
    }

    public String[]  getCco() {
        return cco;
    }

    public void setCco(String[]  cco) {
        this.cco = cco;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
