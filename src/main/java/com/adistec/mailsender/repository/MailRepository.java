package com.adistec.mailsender.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adistec.mailsender.entity.Mail;

public interface MailRepository extends JpaRepository<Mail, Long>{
	List<Mail> findByStatus(String status);
}
