package com.adistec.mailsender.schedule;

import java.io.IOException;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.adistec.mailsender.service.MailService;
import com.adistec.mailsender.service.Impl.MailServiceImpl;

@Component
public class RetryFailedMailsTasks {
	private static final Logger LOG = LoggerFactory.getLogger(RetryFailedMailsTasks.class);

	@Autowired
	MailService mailService;

	@Autowired
	private JavaMailSender officeServer;

	@Autowired
	private JavaMailSender openServer;

	@Scheduled(fixedRate = 1800000)
	public void retryFailedMails() {
		LOG.info("Starting retry failed mails task...");
		mailService.retryFailedMails(officeServer, openServer);
		LOG.info("Retry failed mails task finished.");
	}
}
