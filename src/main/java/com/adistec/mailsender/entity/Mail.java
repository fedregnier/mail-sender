package com.adistec.mailsender.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mail")
public class Mail {
	
	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	private Long id;

	@Column(name = "mail_to")
	private String to;

	@Column(name = "mail_from")
	private String from;

	@Column(name = "mail_subject")
	private String subject;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "sendDate")
	private Date sendDate;

	@Column(name = "attempts")
	private int attempts = 0;

	@Column(name = "isHtml")
	private boolean isHtml;

	@Column(name = "isMultipart")
	private boolean isMultipart;

	@Column(name = "cc")
	private String cc;

	@Column(name = "bcc")
	private String bcc;

	@Column(name = "status")
	private String status;
	
	@Column(name = "inlines")
	private String inlines;
	
	@Column(name="mailServer")
	private String mailServer;

	@Column(name="content")
	private String content;

	public Mail(Long id, String to, String from, String subject, Date createdDate, Date sendDate,
			boolean isHtml, boolean isMultipart, String cc, String bcc, String inlines, String content) {
		this.id = id;
		this.to = to;
		this.from = from;
		this.subject = subject;
		this.createdDate = createdDate;
		this.sendDate = sendDate;
		this.isHtml = isHtml;
		this.isMultipart = isMultipart;
		this.cc = cc;
		this.bcc = bcc;
		this.inlines=inlines;
		this.content=content;
	}
	
	public Mail(String to, String from, String subject, Date createdDate, Date sendDate,
			boolean isHtml, boolean isMultipart, String cc, String bcc, String inlines, String content) {
		this.to = to;
		this.from = from;
		this.subject = subject;
		this.createdDate = createdDate;
		this.sendDate = sendDate;
		this.isHtml = isHtml;
		this.isMultipart = isMultipart;
		this.cc = cc;
		this.bcc = bcc;
		this.inlines=inlines;
		this.content=content;
	}

	public Mail() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public boolean isHtml() {
		return isHtml;
	}

	public void setHtml(boolean isHtml) {
		this.isHtml = isHtml;
	}

	public boolean isMultipart() {
		return isMultipart;
	}

	public void setMultipart(boolean isMultipart) {
		this.isMultipart = isMultipart;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInlines() {
		return inlines;
	}

	public void setInlines(String inlines) {
		this.inlines = inlines;
	}

	public String getMailServer() {
		return mailServer;
	}

	public void setMailServer(String mailServer) {
		this.mailServer = mailServer;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
