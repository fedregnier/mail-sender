package com.adistec.mailsender.service;


import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

@Service
public interface MailService {
	List<String> getTemplateFiles() throws IOException;
	void sendFeedback(String templateVariables, String appName, String subject) throws MessagingException, IOException;
	void sendRecovery(String templateVariables, String subject, String username) throws MessagingException, IOException;
	void sendEmailWithTemplate(String sentToEmail, String subject, String template, String from, String templateVariables, String inlines, String copyTo, String cco) throws JsonParseException, JsonMappingException, IOException, MessagingException;
	void setMailSender(JavaMailSender mailSender);
	void retryFailedMails(JavaMailSender officeServer, JavaMailSender openServer);
	void sendEmailWithContent(String[] sentToEmail, String subject, String content, String from,String inlines, String copyTo[], String cco[]) throws MessagingException, JsonParseException, JsonMappingException, IOException;
}
