package com.adistec.mailsender.controller;

import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;

import com.adistec.mailsender.dto.MailDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.adistec.mailsender.service.MailService;

@RestController
public class MailController {
	private static final Logger LOG = LoggerFactory.getLogger(MailController.class);

	@Autowired
	private MailService mailService;
	
	@RequestMapping(value = "/mail", method = RequestMethod.POST)
	public ResponseEntity<?> sendEmailWithTemplate(@RequestParam(required = false) String sentToEmail, @RequestParam String subject,
			@RequestParam String template, 
			@RequestParam(required = false) String from,
			@RequestParam (required = false) String templateVariables,
			@RequestParam(required = false) String inlines,
			@RequestParam(required = false) String copyTo,
			@RequestParam(required = false) String cco) {
		try {
			LOG.info("Sending email.");
			mailService.sendEmailWithTemplate(sentToEmail, subject, template, from, templateVariables, inlines, copyTo, cco);
			LOG.info("Email sent. ");
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (IOException e) {
			LOG.error("Error sending email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MessagingException e) {
			LOG.error("Error sending email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/mailWithContent", method = RequestMethod.POST)
	public ResponseEntity<?> sendEmailWithContent(@RequestBody MailDTO mailDTO) {
		try {
			LOG.info("Sending email.");
			mailService.sendEmailWithContent(mailDTO.getSentToEmail(),mailDTO.getSubject(),mailDTO.getContent(),mailDTO.getFrom(),mailDTO.getInlines(),mailDTO.getCopyTo(),mailDTO.getCco());
			LOG.info("Email sent. ");
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (IOException e) {
			LOG.error("Error sending email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MessagingException e) {
			LOG.error("Error sending email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	

	@RequestMapping(value = "/mail/feedback", method = RequestMethod.POST)
	public ResponseEntity<?> sendFeedback(@RequestParam String appName,
			 			  				@RequestParam String templateVariables,
			 			  				@RequestParam String subject) {
		try {
			LOG.info("Sending feedback email.");
			mailService.sendFeedback(templateVariables, appName, subject);
			LOG.info("Feedback email sent. ");
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (MessagingException e) {
			LOG.error("Error sending feedback email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IOException e) {
			LOG.error("Error sending feedback email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	

	@RequestMapping(value = "/mail/recovery", method = RequestMethod.POST)
	public ResponseEntity<?> sendRecovery(@RequestParam String templateVariables, @RequestParam String subject, @RequestParam String username) {
		try {
			LOG.info("Sending recovery email.");
			mailService.sendRecovery(templateVariables,subject, username);
			LOG.info("Recovery email sent.");
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (MessagingException e) {
			LOG.error("Error sending recovery email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IOException e) {
			LOG.error("Error sending recovery email. Exception: " + e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/mail/templates", method = RequestMethod.GET)
	public ResponseEntity<?> getTemplates() throws Exception {
		try {
			List<String> templates = mailService.getTemplateFiles();
			return new ResponseEntity<>(templates, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error obteniendo nombres de templates. Exception: " + e.getMessage());
			throw e;
		}
	}
}
